## Teleop with Python

This code is to be run on an external computer (Linux, Mac, Windows) that is on the same network as Misty.

## How can you use it?

1. You can install everything in your computer and run it locally with Misty on the same network as your computer. Follow Stand alone setup instructions found below.

2. You could setup a Raspberry Pi in your place and let that bridge connunication to Misty. This way you can remote-in over the internet. Follow Remote Teleop instructions found below.

## Stand alone Setup-Instructions:

I prefer using anaconda to keep things clean. If your prefer otherwise skip to step 3

### Step 1: (5 mins)

Install Anaconda on your computer following instructions from https://docs.anaconda.com/anaconda/install/. 
Choose your Operating System on the left side menu to get specific instructions.

### Step 2: (1 mins)

Open Terminal 

```
$ conda deactivate 
$ conda create -n mistyTeleop python=3.6 -y
$ conda activate mistyTeleop
```
Now you should see 'mistyTeleop' in brackets before user_name@device_name$ in the terminal

### Step 3: (2 mins)
```
$ pip install requests numpy opencv-python PySimpleGUI grequests av pyaudio Pillow matplotlib
```

## Stand alone Execution-Instructions:

#### If you already know the IP address of Misty:

```
$ python mistyTeleop.py --ip <your_robot's IP>
```

eg. python mistyTeleop.py --ip 10.0.0.237

#### If you do not know the IP address of Misty:


```
$ python mistyTeleop.py
```

Now you would be presented with another window where you could scan the network for Misty's and find the IP addresses.
Scanning can take about 15 seconds.
After scanning is complete you will see a list of buttons one corressponding to each Misty, labelled with IP and SerialNo.
Click on the one Misty you would like to teleoperate and wait 5 seconds for the telepresence interface to kickin.



